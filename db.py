import mysql.connector as mysql
import os
from dotenv import load_dotenv

DEBUG = False


def db_init():
    """
    Initialize the database with mysql.
    """
    load_dotenv()

    # Check if the database exists
    db = mysql.connect(
        host="localhost",
        user="root",
        passwd=str(os.getenv("DB_PASSWORD")),
    )
    db_create_database(db)
    db.close()

    db = mysql.connect(
        host="localhost",
        user="root",
        passwd=str(os.getenv("DB_PASSWORD")),
        database="journal_db",
    )
    return db


def db_create_database(db):
    """
    Create the database.
    """
    cursor = db.cursor()
    sql = """
    CREATE DATABASE IF NOT EXISTS journal_db;
    """
    cursor.execute(sql)
    db.commit()


def db_insert(db, table, col, data):
    """
    Insert data into the database.
    """
    cursor = db.cursor()
    sql = "INSERT INTO {0}({1}) VALUES ({2})".format(table, col, data)
    if DEBUG:
        print(sql)
    cursor.execute(sql)
    db.commit()


def db_select(db):
    """
    Select data from the database.
    """
    cursor = db.cursor()
    sql = "SELECT A.titre,A.contenu, J.nom FROM Article as A JOIN Journal AS J ON A.id_journal = J.id"
    cursor.execute(sql)
    return cursor.fetchall()


def db_update(db, table, data, where):
    """
    Update data in the database.
    """
    cursor = db.cursor()
    sql = "UPDATE {} SET {} WHERE {}".format(table, data, where)
    cursor.execute(sql)
    db.commit()


def db_delete(db, table, data):
    """
    Delete data from the database.
    """
    cursor = db.cursor()
    sql = "DELETE FROM {} WHERE {}".format(table, data)
    cursor.execute(sql)
    db.commit()


def db_create_table(db, table, table_args):
    """
    Create a table in the database.
    """
    cursor = db.cursor()
    sql = """
    CREATE TABLE IF NOT EXISTS {0} (
        id INT NOT NULL AUTO_INCREMENT,
        {1}
    );
    """.format(
        table, table_args
    )
    cursor.execute(sql)
    db.commit()


def db_close(db):
    """
    Close the database.
    """
    db.close()
