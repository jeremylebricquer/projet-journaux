### Imports ###
from urllib import request
from bs4 import BeautifulSoup


### Fonctions ###
def get_page(url):
    ''' Recuperation page web avec Bs4
    Entrée : url d'une page web / Sortie : page web html
    '''
    req = request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = request.urlopen(req).read()
    pages = BeautifulSoup(webpage, "html.parser")
    article = {}
    contenu = []
    
    auteur = pages.find("div", {"class" : "sc-1mpwnru-0 eeZFQT"}).find('a').getText()
    date = pages.find("div", {"class" : "sc-aamjrj-0 sc-1i0ieo8-1 cCZiIu iXMxcQ"}).find('span').getText()
    titre = pages.find("div", {"class" : "sc-aamjrj-0 sc-15kkmyk-0 sc-cyue7g-2 bMXefN dvqCYx"}).find('h1').getText()
    for item in pages.findAll("p", {"class" : "sc-14kwckt-6 hZbEGn"}):
        contenu.append(item.getText())
        article['titre'] = titre
        article['contenu'] = ' '.join(contenu)
        article['date'] = date
        article['auteur'] = auteur
    return article

def get_art_list():
    '''Récupère la liste des url sur la page economie du site 'Les Echos'
    Entrée : page web / Sortie : liste d'urls
    '''
    liste_art = []
    list_url = []
    url = "https://www.lesechos.fr/economie-france"
    req = request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = request.urlopen(req).read()
    page = BeautifulSoup(webpage, "html.parser")
    for i in page.findAll('article', {'class': 'sc-1chlxqy-2 cqINBx'}) :
        listes = i.find('a').get('href')
        liste_art.append(listes)
    for x in liste_art: 
        list_url.append("https://www.lesechos.fr" + x)
    return list_url

def article_list():
    articles = []
    liste_url = get_art_list()
    for i in liste_url:
        articles.append(get_page(i))
    return articles





    