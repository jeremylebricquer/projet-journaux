from urllib.request import Request, urlopen
from bs4 import BeautifulSoup


def get_all_articles():
    '''
        request : https://actu.fr/le-tregor/
        return url list
    '''
    req = Request('https://actu.fr/le-tregor/', headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    page = BeautifulSoup(webpage, "html.parser")
    listes_art =[]
    for listes in page.findAll('li', {'class': 'ac-preview-article ac-preview-article-small'}) :
        listes = listes.find('a').get('href')
        listes_art.append(listes)
    for listes in page.findAll('li', {'class': 'ac-preview-article ac-preview-article-large-h'}) :
        listes = listes.find('a').get('href')
        listes_art.append(listes)
    return listes_art


def article_byUrl(url):
    '''
    Loop on url list
    Recover Title , date ,author , article
    return dict
    '''
    mot_del = ['À lire aussi', 'Partagez','Cet article vous a été utile ? Sachez que vous pouvez suivre Le Trégor dans l’espace Mon Actu . En un clic, après inscription, vous y retrouverez toute l’actualité de vos villes et marques favorites.','Cet article vous a été utile ? Sachez que vous pouvez suivre Le Courrier Indépendant dans l’espace Mon Actu . En un clic, après inscription, vous y retrouverez toute l’actualité de vos villes et marques favorites.']
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    page = BeautifulSoup(webpage, "html.parser")
    article = {}
    contenu = []
    journal = 'Le Tregor'
    title = page.find('title').getText()
    auteur = page.find('div', {'class': 'ac-article-date'}).find('a').getText()
    date = page.find('div', {'class': 'ac-article-date'}).find('time').getText()
    
    for item in page.find('article', {'class': 'js-article-inner'}).findAll({'p'}):
        contenu.append(item.getText())
        for mots in mot_del:
            if mots in contenu:
                contenu.remove(mots)
        article['titre'] = title
        article['contenu'] = ' '.join(contenu)
        article['date'] = date
        article['auteur'] = auteur
        article['journal'] = journal
    return article 

def article():
    '''
        return 1 list with all dict article
    '''
    article = []
    liste_articles = get_all_articles()
    for a in liste_articles:
        article.append(article_byUrl(a))
    return article 



