#### IMPORT ####
import bs4
import pandas as pd
from urllib.request import Request, urlopen

# Get Web page with BS4  

jm_url1 = "https://www.jacquieetmichelblog.fr/"

req = Request(jm_url1, headers={'User-Agent': 'Mozilla/5.0'})
data_JM1 = urlopen(req, cadefault=False).read()
page = bs4.BeautifulSoup(data_JM1, "html.parser")

#### Fonctions ####

#Getting URL list from each articles

def get_name_url():
    url_artJM = []
    for listes in page.findAll('h3', {'class': 'g1-gamma g1-gamma-1st entry-title'}) :
        listes = listes.find('a').get('href')
        url_artJM.append(listes)
    return url_artJM

def get_features(url):
    features = {}
    article_JM = []
    journal = 'JM'

    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req, cadefault=False).read()
    page = bs4.BeautifulSoup(webpage, "html.parser")
    texte1 = page.findAll("div",{"class" : "g1-wrapper-with-stickies"})
    
    try:
        titre = page.find("title").getText()
        date = page.find("time",{"class" : "entry-date"})['datetime']
        auteur = page.find("span",{"class" : "entry-author"}).find('a').getText()
    except: 
        titre = "NA"
        date = "NA"
        auteur = "NA"
        #print(url)

    for item in texte1:
        article_JM.append(item.getText())
        features['titre'] = titre
        features['contenu'] = ' '.join(article_JM).strip()
        features['date'] = date
        features['auteur'] = auteur.strip()
        features['journal'] = journal

    return features

def get_all_name():
    name_list = []
    listes_nom = get_name_url()
    for a in listes_nom : 
        name_list.append(get_features(a)) 
    return name_list



