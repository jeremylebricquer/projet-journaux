from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
import pandas as pd

DEBUG = False


def get_all_articles():
    '''
        request : https://www.lemonde.fr/
    '''

    req = Request('https://www.lemonde.fr/', headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    webpage = webpage.decode('utf-8')
    page = BeautifulSoup(webpage, "html.parser")

    list_articles = []
    for listes in page.findAll('div', {'class': 'article'}):
        article_url = listes.find('a').get('href')
        list_articles.append(article_url)

    return list_articles


def article_byUrl_lemonde(url):
    '''
    Loop on url list
    Recover Title , date ,author , article
    return dict
    '''
    mot_del = ['Lire aussi', 'Partagez','Cet article vous a été utile ? Sachez que vous pouvez suivre Le Trégor dans l’espace Mon Actu . En un clic, après inscription, vous y retrouverez toute l’actualité de vos villes et marques favorites.','Cet article vous a été utile ? Sachez que vous pouvez suivre Le Courrier Indépendant dans l’espace Mon Actu . En un clic, après inscription, vous y retrouverez toute l’actualité de vos villes et marques favorites.']
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    page = BeautifulSoup(webpage, "html.parser")
    article = {}
    contenu = []
    journal = 'Le Monde'

    try:
        title = page.find('h1', {'article__title'}).getText()
        auteur = page.find('p', {'class': 'author__detail'}).find('span').getText()
        date = page.find('span', {'class': 'meta__date'}).getText()
    except:
        title = 'NA'
        auteur = 'NA'
        date = 'NA'
        if DEBUG:
            print(url)

    try:
        for item in page.find('article', {'class': 'article__content'}).findAll({'p', 'h2'}):
            contenu.append(item.getText())
            for mots in mot_del:
                if mots in contenu:
                    contenu.remove(mots)
            
    except:
        contenu = 'NA'
        if DEBUG:
            print(url)
    
    article['titre'] = title
    article['contenu'] = ' '.join(contenu)
    article['date'] = date
    article['auteur'] = auteur
    article['journal'] = journal

    return article


def article_lemonde():
    '''
        return list with all dict article
    '''
    article = []
    liste_articles = get_all_articles()
    for a in liste_articles:
        art = article_byUrl_lemonde(a)
        if art.get('titre') != 'NA' and art.get('contenu') != 'NA':
            article.append(art)
    return article 

if __name__ == '__main__':
    if DEBUG:
        article_byUrl_lemonde('https://www.lemonde.fr/economie/article/2022/03/08/deliveroo-et-trois-de-ses-anciens-dirigeants-juges-au-penal-pour-travail-dissimule_6116542_3234.html')
    #get_all_articles()