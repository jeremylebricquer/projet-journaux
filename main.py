import pandas as pd
import Scrapping.Tregor as art_letregor
import Scrapping.LeMonde as art_lemonde
import Scrapping.Journal_JM as art_jm
import Scrapping.Echos as art_je

from db import *

def create_database(df):
    db = db_init()
    db_create_table(db, 'Journal', 'nom VARCHAR(255), PRIMARY KEY(id)')
    db_create_table(db, 'Article', 'titre TEXT, contenu TEXT, date_publication VARCHAR(255), auteur VARCHAR(255), id_journal integer, FOREIGN KEY(id_journal) REFERENCES Journal(id), PRIMARY KEY (id)')

    db_insert(db, 'Journal', '`nom`', '"Le Tregor"')
    db_insert(db, 'Journal', '`nom`', '"Le Monde"')
    db_insert(db, 'Journal', '`nom`', '"JM"')
    db_insert(db, 'Journal', '`nom`', '"Les Echos"')
    df = df.to_dict(orient="list")

    for i in range(len(df['titre'])):

        db_insert(db, 'Article', '`titre`, `contenu`, `date_publication`, `auteur`, `id_journal`' , '"{}", "{}", "{}", "{}", {}'.format(str(df['titre'][i]).replace('"', "'"), str(df['contenu'][i]).replace('"', "'"), df['date'][i], df['auteur'][i], df['id_journal'][i]))

def encode_journal(journal):
    if journal == 'Le Tregor':
        return 1
    elif journal == 'Le Monde':
        return 2
    elif journal == 'JM':
        return 3
    elif journal == 'Les Echos':
        return 4
    else:
        return 0

def requests():
    db = db_init()
    features =db_select(db)
    df = pd.DataFrame(features)
    return df

def scrapping():
    # Le Tregor
    d = art_letregor.article()
    df = pd.DataFrame(d)
    df.to_csv('Data/Tregor.csv',index=False)

    # Le Monde
    d = art_lemonde.article_lemonde()
    df = pd.DataFrame(d)
    df.to_csv('Data/LeMonde.csv', index=False)

    # JM
    d = art_jm.get_all_name()
    df = pd.DataFrame(d)
    df.to_csv('Data/jmblog.csv', index=False)

    # Echos
    echos = art_je.article_list()
    df = pd.DataFrame(echos, columns =['titre', 'contenu', 'date', 'auteur', 'journal'])
    df['journal'] = df['journal'].fillna('Les Echos')
    df.to_csv('Data/Les_Echos.csv',index=False)
    ### Concat
    df_echos = pd.read_csv('Data/Les_Echos.csv')
    df_monde = pd.read_csv('Data/LeMonde.csv')
    df_jm = pd.read_csv('Data/jmblog.csv')
    df_tregor = pd.read_csv('Data/Tregor.csv')
    df_articles = pd.concat([df_tregor, df_monde, df_jm, df_echos])

    

    df_articles['id_journal'] = df_articles['journal'].apply(encode_journal)
    df_articles.dropna(inplace=True)
    df_articles.to_csv('Data/articles.csv', index=False)

    return df_articles

if __name__ == ('__main__'):
    if os.path.isfile('Data/articles.csv'):
        df = pd.read_csv('Data/articles.csv')
    else:
        df = scrapping()

    create_database(df)
    requests()
